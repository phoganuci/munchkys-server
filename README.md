Munchkys Server
===============

A simple implementation of a Node.js REST API built with Express and Mongoose, using Passport for authentication.

Installation
------------

- Install [MongoDB](http://www.mongodb.org/), [node.js](http://nodejs.org/) and [npm](http://npmjs.org/)
- Clone the repository to your computer
- Install dependancies: `npm install`
- Start MongoDB: `sudo mongod`
- Generate test data: 'node [src_root]/test/dataGen.js'
- Start the server: `node server.js`

Testing Endpoints
-----------------

- Install Postman Google Chrome plugin
- Import [src_root]/test/postman.json collection
- Create user using Users::Create endpoint
- Test OAuth2 flow using Tokens::Grant and Tokens::Refresh endpoints
- Test user lookup with Users::Lookup endpoint