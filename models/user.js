var mongoose = require('mongoose');
var crypto = require('crypto');

var Schema = mongoose.Schema;
var UserSchema = new Schema({
    username: {
        type: String,
        unique: true,
        required: true
    },
    hashedPassword: {
        type: String,
        required: true
    },
    salt: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    }
});

UserSchema.virtual('type')
    .get(function () {
        return 'user';
    });

UserSchema.virtual('password')
    .set(function (password) {
        this.salt = crypto.randomBytes(128).toString('base64');
        this.hashedPassword = this.encryptPassword(password);
    });

UserSchema.methods.encryptPassword = function (password) {
    var key = crypto.pbkdf2Sync(password, this.salt, 10000, 512).toString('base64');
    return key;
};

UserSchema.methods.checkPassword = function (password) {
    return this.encryptPassword(password) === this.hashedPassword;
};

UserSchema.methods.toJSON = function () {
    var JSONObject = this.toObject();
    JSONObject.id = this.id;
    JSONObject.type = this.type;

    delete JSONObject._id;
    delete JSONObject.__v;
    delete JSONObject.hashedPassword;
    delete JSONObject.salt;
    delete JSONObject.created;

    return JSONObject;
};

module.exports.UserSchema = mongoose.model('User', UserSchema);