module.exports = function (server, config) {
    function requireRoutes(relativePath) {
        require(config.routes_path + relativePath)(server, config);
    };

    requireRoutes('/routes-auth');
    requireRoutes('/routes-me');
    requireRoutes('/routes-user');
}