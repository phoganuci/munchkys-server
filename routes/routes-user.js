var passport = require('passport');

module.exports = function (server, config) {
    var log = require(config.utils_path + '/log')(module);
    var User = require(config.models_path + '/user').UserSchema;

    // Users::Create
    server.post(config.api_base_path + '/user', function (req, res) {
        User.findOne({ username: req.body.username }, function (err, existingUser) {
            if (err) {
                log.error("Error querying user: " + err);
                res.send({ error: { code: 'InternalError', message: err.message } });
            }
            else if (existingUser) {
                res.status(409);
                res.send({ error: { code: 'ResourceAlreadyExists', message: 'This username already exists.' } });
                log.info("Username already exists: " + req.body.username.username);
            }
            else {
                var user = new User({
                    username: req.body.username,
                    password: req.body.password
                });

                log.info("Creating new user - %s", user.username);
                user.save(function (err, user) {
                    if (err) {
                        log.error("Error saving new user: " + err);
                        next();
                    }
                    else {
                        res.send(user.toJSON());
                    }
                });
            }
        });
    });

    // Users::Lookup
    server.get(config.api_base_path + '/user/:id', function (req, res) {
        User.findById(req.params.id, function (err, user) {
            if (err || !user) {
                res.status(404);
                res.send({ error: { code: 'ResourceNotFound', message: 'User not found' } });
            }
            else {
                res.send(user.toJSON());
            }
        });
    });
}