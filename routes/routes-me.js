var passport = require('passport');

module.exports = function (server, config) {
    // Me::Info
    server.get(config.api_base_path + '/me', passport.authenticate('bearer', { session: false }), function (req, res) {
        var JSONObject = req.user.toJSON();
        JSONObject.scope = req.authInfo.scope;
        res.json(JSONObject);
    });
}