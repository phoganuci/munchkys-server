module.exports = function (server, config) {
    server.post('/oauth/token', require(config.auth_path + '/oauth2')(config));
}