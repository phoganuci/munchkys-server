// Load configurations
var config = require('../config');

// Packages
var faker = require('Faker');

// Utilities
var log = require(config.utils_path + '/log')(module);

// Models
var User = require(config.models_path + '/user').UserSchema;
var Client = require(config.models_path + '/client').ClientSchema;
var AccessToken = require(config.models_path + '/access_token').AccessTokenSchema;
var RefreshToken = require(config.models_path + '/refresh_token').RefreshTokenSchema;

// Database Setup
var mongoose = require('mongoose');
mongoose.connect(config.db_url);

User.remove({}, function (err) {
    for (i = 0; i < 4; i++) {
        password = faker.Lorem.words(1)[0];
        var user = new User({ username: faker.random.first_name().toLowerCase(), password: password });
        log.info("Creating new user - %s:%s", user.username, password);
        user.save(function (err, user) {
            if (err) {
                return log.error(err);
            }
            else {
                log.info("Success creating new user - %s", user.username);
            }
        });
    }
});

Client.remove({}, function (err) {
    var client = new Client({ name: "ios_client_v1", clientId: "13e63ab5f2cf3b36b9e13790e92744d", clientSecret: "1d78197b901c233bae579360805679bd" });
    client.save(function (err, client) {
        if (err) {
            return log.error(err);
        }
        else {
            log.info("New client - %s:%s", client.clientId, client.clientSecret);
        }
    });
});
AccessToken.remove({}, function (err) {
    if (err) {
        return log.error(err);
    }
});
RefreshToken.remove({}, function (err) {
    if (err) {
        return log.error(err);
    }
});

setTimeout(function () {
    mongoose.disconnect();
}, 3000);