// Load configurations
var config = require('./config');

// Packages
var express = require('express');
var passport = require('passport');
var mongoose = require('mongoose');

// Utilities
var log = require('./utils/log')(module);

// Database setup
mongoose.connect(config.db_url);
var db = mongoose.connection;
db.on('error', function (err) {
    log.error('connection error:', err.message);
});
db.once('open', function callback() {
    log.info("Connected to DB!");
});

// Server setup
var server = express();
server.use(express.logger('dev'));
server.use(express.bodyParser());
server.use(passport.initialize());
server.use(express.methodOverride());
server.use(server.router);

server.use(function (req, res, next) {
    res.status(404);
    log.debug('Not found URL: %s', req.url);
    res.send({ error: { code: 'ResourceNotFound', message: 'Resource not found' } });
    return;
});

server.use(function (err, req, res, next) {
    res.status(err.status || 500);
    log.error('Internal error(%d): %s', res.statusCode, err.message);
    res.send({ error: { code: 'InternalError', message: err.message } });
    return;
});

// Authentication
require(config.auth_path + '/auth')(passport, config);

// Routes
require('./routes/routes')(server, config);

// Start server
server.listen(config.port, function () {
    log.info('Express server listening on port ' + config.port);
});