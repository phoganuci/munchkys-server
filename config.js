var _ = require('underscore');

var root = require('path').normalize(__dirname);

var shared = {
    root_path: root ,
    models_path: root + '/models',
    routes_path: root + '/routes',
    auth_path: root + '/auth',
    utils_path: root + '/utils',
    api_base_path: '/api'
}

var environments = {
    development: {
        host: 'localhost',
        port: '3412',
        db_url: 'mongodb://localhost/munchkys',
        session_timeout: 3600, // 1 hr
        version: '1.0.0'
    }
}

module.exports = _.extend(shared, environments[process.env.NODE_ENV || 'development']);