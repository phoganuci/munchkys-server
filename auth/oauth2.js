module.exports = function (config) {
    var oauth2orize = require('oauth2orize');
    var passport = require('passport');
    var crypto = require('crypto');
    var User = require(config.models_path + '/user').UserSchema;
    var AccessToken = require(config.models_path + '/access_token').AccessTokenSchema;
    var RefreshToken = require(config.models_path + '/refresh_token').RefreshTokenSchema;

    // create OAuth 2.0 server
    var server = oauth2orize.createServer();

    // Exchange username & password for access token.
    server.exchange(oauth2orize.exchange.password(function (client, username, password, scope, done) {
        User.findOne({ username: username }, function (err, user) {
            if (err) {
                return done(err);
            }
            if (!user) {
                return done(null, false);
            }
            if (!user.checkPassword(password)) {
                return done(null, false);
            }

            RefreshToken.remove({ userId: user.id, clientId: client.clientId }, function (err) {
                if (err) {
                    return done(err);
                }
            });
            AccessToken.remove({ userId: user.id, clientId: client.clientId }, function (err) {
                if (err) {
                    return done(err);
                }
            });

            var tokenValue = crypto.randomBytes(32).toString('base64');
            var refreshTokenValue = crypto.randomBytes(32).toString('base64');
            var token = new AccessToken({ token: tokenValue, clientId: client.clientId, userId: user.id });
            var refreshToken = new RefreshToken({ token: refreshTokenValue, clientId: client.clientId, userId: user.id });
            refreshToken.save(function (err) {
                if (err) {
                    return done(err);
                }
            });
            var info = { scope: '*' }
            token.save(function (err, token) {
                if (err) {
                    return done(err);
                }
                done(null, tokenValue, refreshTokenValue, { 'expires_in': config.session_timout });
            });
        });
    }));

    // Exchange refreshToken for access token.
    server.exchange(oauth2orize.exchange.refreshToken(function (client, refreshToken, scope, done) {
        RefreshToken.findOne({ token: refreshToken }, function (err, token) {
            if (err) {
                return done(err);
            }
            if (!token) {
                return done(null, false);
            }
            if (!token) {
                return done(null, false);
            }

            User.findById(token.userId, function (err, user) {
                if (err) {
                    return done(err);
                }
                if (!user) {
                    return done(null, false);
                }

                RefreshToken.remove({ userId: user.id, clientId: client.clientId }, function (err) {
                    if (err) {
                        return done(err);
                    }
                });
                AccessToken.remove({ userId: user.id, clientId: client.clientId }, function (err) {
                    if (err) {
                        return done(err);
                    }
                });

                var tokenValue = crypto.randomBytes(32).toString('base64');
                var refreshTokenValue = crypto.randomBytes(32).toString('base64');
                var token = new AccessToken({ token: tokenValue, clientId: client.clientId, userId: user.id });
                var refreshToken = new RefreshToken({ token: refreshTokenValue, clientId: client.clientId, userId: user.id });
                refreshToken.save(function (err) {
                    if (err) {
                        return done(err);
                    }
                });
                var info = { scope: '*' }
                token.save(function (err, token) {
                    if (err) {
                        return done(err);
                    }
                    done(null, tokenValue, refreshTokenValue, { 'expires_in': config.session_timout });
                });
            });
        });
    }));

    return [
        passport.authenticate(['basic', 'oauth2-client-password'], { session: false }),
        server.token(),
        server.errorHandler()
    ]
};